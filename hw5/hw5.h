#ifndef HW5_H
#define HW5_H

#define N 7
#define NSIZE (N + 1)

double x[NSIZE];
double function[NSIZE];
double b_i[NSIZE];

int i;
double temp, sum, truevalue, abvalue;

double B[NSIZE][NSIZE];
double BINV[NSIZE][NSIZE];
double weights[NSIZE];
double I[NSIZE][NSIZE];
double copy[NSIZE][NSIZE];

#endif
