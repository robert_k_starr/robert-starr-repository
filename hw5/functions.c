#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "mkl.h"
#include "functions.h"
#include "hw5.h"

void fill(double *box, double x){	//Fills a row in with the proper basis terms
	int i;
	for (i = 0; i < NSIZE; i++){
		box[i] = pow(x, i);
	}
}

void fill_integral(double *box, double x){
	int i;
	for (i = 0; i < NSIZE; i++) box[i] = 1.0/((double) i + 1)*pow(x, i + 1);
}

void fill_derivative(double *box, double x, int order){
	int i;
	for (i = 0; i < NSIZE; i++){
		int temporder;
		double tempvalue = 1;
		for (temporder = 0; temporder < order; temporder++) tempvalue = tempvalue*(i - temporder);
		if (x == 0) box[i] = 0;
		else box[i] = tempvalue*pow(x, i - order);
	}
}

void printmatrix(double *matrix, int row, int column){
	int i, j;
	for (i = 0; i < row; i++){
		for (j = 0; j < column; j++){
			printf("%f\t", matrix[i*column + j]);
		}
		printf("\n");
	}
	printf("\n");
}

void identity(double *matrix, int size){
	int i, j;
	for (i = 0; i < size; i++){
		for (j = 0; j < size; j++){
			if (i == j) matrix[i*size + j] = 1;
			else matrix[i*size + j] = 0;
		}
	}
}

void copymatrix(double *original, double *copy, int row, int column){
	int i, j;
	for (i = 0; i < row; i++){
		for (j = 0; j < column; j++) copy[i*column + j] = original[i*column + j];
	}
}

void assignweights(double *c, int k_max, double *f, double *B, int j_max){
	int j, k;
	for (k = 0; k < k_max; k++){
		double sum = 0;
		for (j = 0; j < j_max; j++){
			sum = sum + f[j]*B[j*k_max + k];
		}
		c[k] = sum;
	}
}

void interpolate(double *b, double *BINV, double *weights, double *function, int size, double truevalue){
	
	double sum = 0, abvalue;	
	
	assignweights(weights, size, b, BINV, size);

	printmatrix(weights, size, 1);

	for (i = 0; i < size; i++) sum = sum + weights[i]*function[i];
	
	abvalue = abs(sum - truevalue);
	
	printf("Check the interpolation accuracy\n\n");	
	printf("x=0.5\tnumerical value\t\tanalytical value\tabsolute value\n");
	printf(".5\t%.8f\t\t%.8f\t\t%.8f\n\n", sum, truevalue, abvalue);
}
