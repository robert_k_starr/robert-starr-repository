#ifndef FUNCTIONS_H
#define FUNCTIONS_H

void fill(double *, double);
void fill_integral(double *, double);
void fill_derivative(double *, double, int);
void printmatrix(double *, int, int);
void identity(double *, int);
void copymatrix(double *, double *, int, int);
void assignweights(double *, int, double *, double *, int);
void interpolate(double *, double *, double *, double *, int, double);

#endif
