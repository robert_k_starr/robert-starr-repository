#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "mkl.h"
#include "hw5.h"
#include "functions.h"

int main(){

	identity(*I, NSIZE);

	printf("i\tx\t\t\tf(x)\n");
	for (i = 0; i < NSIZE; i++){
		temp 		= 0.0 + 1.0*i/7;
		x[i]		= temp;
		function[i] 	= pow(temp, N); 
		printf("%i\t%.15f\t%.15f\n", i, x[i], function[i]);
	}
	
	printf("\n\n");

	// Fill in B Matrix
	
	for (i = 0; i < NSIZE; i++){
		fill(B[i], x[i]);
	}
	
	printf("B Matrix:\n");	

	printmatrix(*B, NSIZE, NSIZE);
	copymatrix(*I, *BINV, NSIZE, NSIZE);
	copymatrix(*B, *copy, NSIZE, NSIZE);

	MKL_INT ipiv[NSIZE];	
	
	// Create B^-1 Matrix
	
	LAPACKE_dgesv( LAPACK_ROW_MAJOR, NSIZE, NSIZE, *copy, NSIZE, ipiv, *BINV, NSIZE);

	printf("B^-1 Matrix:\n");

	printmatrix(*BINV, NSIZE, NSIZE);

	// Begin interpolations

	fill(b_i, .5 );

	interpolate(b_i, *BINV, weights, function, NSIZE, pow(.5, 7));

	fill_derivative(b_i, .5, 1);

	printf("Weights for 1st order differentiation\n\n");

	interpolate(b_i, *BINV, weights, function, NSIZE, 7*pow(.5, 6));

	fill_derivative(b_i, .5, 2);

	printf("Weights for second order differentiation\n\n");

	interpolate(b_i, *BINV, weights, function, NSIZE, 42*pow(.5, 5));

	fill_integral(b_i, .5);

	printf("Weights for integration\n\n");

	interpolate(b_i, *BINV, weights, function, NSIZE, 1.0/8.0*pow(.5,8));

}
