#include <stdio.h>
#define N 2048
int i, j, k;
double C[N][N], A[N][N], B[N][N];

int main()
{
	for (i = 0; i < N; i++){
		for (j = 0; j < N; j++){
			C[i][j] = 0;
			A[i][j] = 1;
			B[i][j] = 1;	
		}
	}
	
	for (i = 0; i < N; i++){
		for (j = 0; j < N; j++){
			for (k = 0; k < N; k++){
				C[i][j] = C[i][j] + A[i][k]*B[k][j];
			}
		}
	}
	return 0;
}
