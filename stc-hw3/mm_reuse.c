#include <stdio.h>
#define N 2048
int i, j, k;
double C[N][N], A[N][N], B[N][N];
double sum;

int main()
{
	for (i = 0; i < N; i++){
		for (j = 0; j < N; j++){
			C[i][j] = 0;
			A[i][j] = 1;
			B[i][j] = 1;	
		}
	}
	
	for (i = 0; i < N; i++){
		for (j = 0; j < N; j++){
			sum = 0;
			for (k = 0; k < N; k++){
				sum = sum + A[i][k]*B[k][j];
			}
			C[i][j] = sum;
		}
	}
	return 0;
}
