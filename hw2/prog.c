#include <math.h>
#include <stdio.h>
#include <stdlib.h>

void isqrt(float *a,int n), sums(int);
double midp(float, float), trap(float, float), simp(float, float);
double truev(float, float);

main(){
	int i, N = 7;
	printf("\n");
	
	sums(0);
	for (i = 1; i < N; i++) sums(pow(10, i) + 1);

	return 0;
}
