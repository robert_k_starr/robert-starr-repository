#include <math.h>
#include <stdio.h>
#include <time.h>
#include "parameters.h"

void isqrt(float *a,int n){
	int i;
//			Vector inverse square
//			Someone advised to
//			making two separate loops
	for(i=0;i<n;i++) a[i] = sqrt(a[i]);
	for(i=0;i<n;i++) a[i] = 1.0e0/a[i];
}

double midp( float xi, float h){
	return pow( (xi+h*0.5e0),(double)7);
}

double trap( float xi, float h){
	return (pow( xi, (double)7 ) + pow( (xi+h), (double) 7))/2;
}

double simp( float xi, float h){
	return (pow( (xi - h),(double)7 )+4*pow( xi, (double)7 ) + pow( (xi+h),(double) 7 ))/3;
}

double truev( float xi, float xf){
	return (double) 1/8 * (pow(xf, (double)8) - pow(xi, (double)8));
}

void sums(int N){
	double h = (double) (x_end - x_start)/N;
	int i;

	double M, x;
	double midp_i = 0, trap_i = 0, simp_i = 0;
	double midp_err = 0, trap_err = 0, simp_err = 0;
	double midp_time, trap_time, simp_time;

	clock_t start, end;
	
	if (N == 0) {
		printf("N\tTrue\t\tMidpt\t\tTrapz\t\tSimps\t\t");
		printf("err_Mdpt\terr_Trapz\terr_Simps\t");
		printf("time_Mdpt\ttime_Trapz\ttime_Simps\n");
		return;}

	start = clock();

	for (i=0; i < N; i++){
		x = x_start + i*h;
		midp_i = midp_i + h*midp(x, h);
	}
	
	end = clock();
	
	midp_time = ((double) (end - start)) / CLOCKS_PER_SEC;

	start = clock();

	for (i=0; i < N; i++){
		x = x_start + i*h;
		trap_i = trap_i + h*trap(x, h);
	}

	end = clock();

	trap_time = ((double) (end - start)) / CLOCKS_PER_SEC;

	start = clock();

	for (i=0; i < N; i++){
		x = x_start + i*h;
		if (!(i%2)) simp_i = simp_i + h*simp(x, h);
	}

	end = clock();

	simp_time = ((double) (end - start)) / CLOCKS_PER_SEC;

	M = truev(x_start, x_end);
	
	midp_err = M - midp_i;
	trap_err = M - trap_i;
	simp_err = M - simp_i;

	printf("%i\t%f\t%.8f\t%.8f\t%.8f\t", N, M, midp_i, trap_i, simp_i);
	printf("%.12f\t%.12f\t%.12f\t", midp_err, trap_err, simp_err);
	printf("%.12f\t%.12f\t%.12f\n", midp_time, trap_time, simp_time);
}
