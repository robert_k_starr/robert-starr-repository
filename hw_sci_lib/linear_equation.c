#include <stdlib.h>
#include <stdio.h>
#include "mkl.h"

#define N 3
#define NRHS 1
#define LDA N
#define LDB NRHS

int main()
{
	MKL_INT n = N, nrhs = NRHS, lda = LDA, ldb = LDB;
	
	MKL_INT ipiv[N];
	
	double A[N*N] = {
		6,	-2,	2,
		12,	-8,	6,
		3,	-13,	3
	};
	
	double b[NRHS*N] = {
		16,
		26,
		-19
	};

	LAPACKE_dgesv( LAPACK_ROW_MAJOR, n, nrhs, A, lda, ipiv, b, ldb );
	
	printf("\n%f\n%f\n%f\n\n", b[0], b[1], b[2]);
	
	return 0;
}
