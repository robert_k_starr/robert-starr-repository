#include <stdio.h>
#include <stdlib.h>
#include "mkl.h"

#define N 5
#define NRHS N
#define LDA N
#define LDB N
#define LDC N

void printmatrix(int rows, int columns, double matrix[]){
	int i, j;
	for(i = 0; i < columns; i++){
		for(j = 0; j < rows; j++){
			printf("%e\t", matrix[i*rows + j]);
		}
		printf("\n");
	}
	printf("\n");
}

int copymatrix(int rows, int columns, double matrix1[], double matrix2[]){
	int i, j;
	for(i = 0; i< columns; i++){
		for(j = 0; j < rows; j++){
			matrix2[i*rows + j] = matrix1[i*rows + j];
		}
	}
	return 1;
}

int main(){
	int i, j;

	MKL_INT n = N, nrhs = NRHS, lda = LDA, ldb = LDB, ldc = LDC, info;	
	MKL_INT ipiv[N];

	double A[5*5] = {
		1.0,	1.0/2,	1.0/3,	1.0/4,	1.0/5,
		1.0/2,	1.0/3,	1.0/4,	1.0/5,	1.0/6,
		1.0/3,	1.0/4,	1.0/5,	1.0/6,	1.0/7,
		1.0/4,	1.0/5,	1.0/6,	1.0/7,	1.0/8,
		1.0/5,	1.0/6,	1.0/7,	1.0/8,	1.0/9
	};

	double I[N*N], B[N*N];
	for (i = 0; i < N; i++){
		for (j = 0; j < N; j++){
			if (i != j) I[i*N + j] = 0;
			else I[i*N + j] = 1.0;
		}
	}
	
	copymatrix(N, N, I, B);	
	
	double copy[N*N];

	copymatrix(N, N, A, copy);

	info = LAPACKE_dgesv( LAPACK_ROW_MAJOR, n, nrhs, copy, lda, ipiv, B, ldb);
	
	printf("\nA\n\n");

	printmatrix(N, N, A);	
	
	printf("\n\nA^(-1)\n\n");

	printmatrix(N, N, B);
	
	double C[N*N];

	cblas_dgemm( CblasRowMajor, CblasNoTrans, CblasNoTrans, N, N, N, 1, A, lda, B, ldb, 0, C, ldc);	
	
	printf("\n\nA*A^(-1)\n\n");
	printmatrix(N, N, C);
	
	cblas_dgemm( CblasRowMajor, CblasNoTrans, CblasNoTrans, N, N, N, 1, B, ldb, A, lda, 0, C, ldc);

	printf("\n\nA^(-1)*A\n\n");
	printmatrix(N, N, C);
	
	return 0;
}
