#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#include "random.h"

void initiate(){
	srand((unsigned) time(NULL));
}

double rand_range(double min, double max){
	double number, range;
	int precision = pow(10, DIGITS);
	
	range = max - min;
	
	number = (double) (rand() % precision + 1);
	number = range*number/((double) precision) + min;
	
	return number;
}

double inversesampling(double min, double lambda){
	return -(log(rand_range(0, 1))/lambda);
}

double normal(double input, double mu, double sigma){
	double ratio = 1.0/(sigma*sqrt(2*PI));
	double function = exp(-pow((input - mu), 2)/(2*pow(sigma, 2)));
	
	return ratio*function;
}

double accrej(double min, double max, double mu, double sigma){
	while (1){
		double Y = rand_range(min, max);
		double U = rand_range(0, 1);
	
		if (U <= normal(Y, mu, sigma)*sigma*sqrt(2*PI)) return Y;
	}
}

double function(double x){
	return exp(-pow(x, 2)/2)*(cos(x) + 5);
}

double modifiedfunction(double x){
	return sqrt(2*PI)*(cos(x) + 5);
}