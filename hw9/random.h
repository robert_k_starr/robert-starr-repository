#ifndef RANDOM_H
#define RANDOM_H

#define DIGITS 8
#define PI 3.141592653589793

void initiate();
double rand_range(double, double);
double inversesampling(double, double);

double normal(double, double, double);
double accrej(double, double, double, double);

double function(double);
double modifiedfunction(double);

#endif /* RANDOM_H */