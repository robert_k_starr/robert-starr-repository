#include <stdio.h>
#include <stdlib.h>

#include "rand_exp.h"
#include "../random.h"
#include "../output.h"

int main(){

	int i;
	
	initiate();
	
	FILE *file;
	
	file = fopen("rand_exp.dat", "w");
	
	for (i = 0; i < N; i++) outputnumber(file, inversesampling(MIN, 1.0/3));
	
	fclose(file);
	
	return 0;
}