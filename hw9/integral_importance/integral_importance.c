#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "../random.h"
#include "../output.h"

#define N 5

double integrate(int);

int main(){

	int i;
	
	double values[N];
	
	initiate();
	for (i = 0; i < N; i++) values[i] = integrate((int) pow(10, i+1));
	
	returnvalues(values, N);

	return 0;

}

double integrate(int n){
	double sum = 0;
	int i;
	
	double x = accrej(-5, 5, 0, 1);
	
	for (i = 0; i < n; i++) sum = sum + modifiedfunction(x);
	
	sum = sum/n;
	
	return sum;
}