#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "../random.h"
#include "../output.h"
#include "integral_unbiased.h"

int main(){

	int i;
	
	double values[N];
	
	initiate();
	
	for (i = 0; i < N; i++) values[i] = integrate((int) pow(10, i+1));
	
	returnvalues(values, N);
	
	return 0;
}

double integrate(int n){
	double frac = 0, x, y;
	int i;
	
	for (i = 0; i < n; i++){
		x = rand_range(-5, 5);
		y = rand_range(0, 6);
		
		if (y < function(x)) frac++;
	}
	
	frac = frac/n;
	
	return frac*10*6;
}