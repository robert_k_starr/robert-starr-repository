#include <stdio.h>
#include <stdlib.h>

#include "rand_uniform.h"
#include "../random.h"
#include "../output.h"

int main(){

	int i;
	
	initiate();
	
	FILE *file;
	
	file = fopen("rand_uniform.dat", "w");
	
	for (i = 0; i < N; i++) outputnumber(file, rand_range(MIN, MAX));
	
	fclose(file);
	
	return 0;
}