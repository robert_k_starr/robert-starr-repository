#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "pi.h"
#include "../random.h"
#include "../output.h"

int main(){

	int i;
	
	initiate();
	
	double pivalues[5];
	
	for (i = 0; i < 5; i++) pivalues[i] = approximate(pow(10, i+1));
	
	returnvalues(pivalues, 5);
	
	return 0;
}

double approximate(int n){
	int i;
	int success = 0;
	
	for (i = 0; i < n; i++){
		double x, y;
		x = rand_range(0, 1);
		y = rand_range(0, 1);
		if ((pow(x, 2) + pow(y, 2)) <= 1) success++;
	}
	
	return 4*((double) success)/n;
}