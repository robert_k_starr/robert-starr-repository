#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

void outputnumber(FILE *file, double value){
	
	fprintf(file, "%f\n", value);
	
}

void returnvalues(double *values, int n){
	int i;

	for(i = 0; i < n; i++) printf("%f\t%f\n", pow(10,i +1), values[i]);
}