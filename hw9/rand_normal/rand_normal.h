#ifndef RAND_NORMAL
#define RAND_NORMAL

#define N 100000
#define MIN -5
#define MAX 5

#define MU 0
#define SIGMA 1

#endif /* RAND_NORMAL */