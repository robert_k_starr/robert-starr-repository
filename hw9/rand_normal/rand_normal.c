#include <stdio.h>
#include <stdlib.h>

#include "rand_normal.h"
#include "../random.h"
#include "../output.h"

int main(){

	int i;
	
	initiate();
	
	FILE *file;
	
	file = fopen("rand_normal.dat", "w");
	
	for (i = 0; i < N; i++) outputnumber(file, accrej(MIN, MAX, MU, SIGMA));
	
	fclose(file);
	
	return 0;
}