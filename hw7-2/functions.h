#ifndef FUNCTIONS_H
#define FUNCTIONS_H

int i, j;

double dotproduct(double *, double *, int);
void projection(double *, double *, int);
double norm(double *, int);
void normalize(double *, int);
void printvector(double *, int);
void subtract(double *, double *, int);

void gramschmidt(double *, double *, double *, int);

#endif /*FUNCTIONS_H*/
