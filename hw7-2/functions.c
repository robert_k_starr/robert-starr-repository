#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "functions.h"

double dotproduct(double *a, double *b, int n){
	double sum = 0;
	for (i = 0; i < n; i++) sum = sum + a[i]*b[i];
	return sum;
}

void projection(double *a, double *b, int n){ 		// Projects a onto b, returning a as the projection
	double fraction = dotproduct(a, b, n)/norm(b, n);
	for (i = 0; i < n; i++) a[i] = fraction*b[i];
}

double norm(double *a, int n){
	double square = 0;
	for (i = 0; i < n; i++) square = square + pow(a[i], 2);
	return square;
}

void normalize(double *a, int n){
	double normvector = norm(a, n);
	for (i = 0; i < n; i++) a[i] = a[i]/sqrt(normvector);
}

void printvector(double *a, int n){
	printf("\n(");
	for (i = 0; i < n; i++) printf("%f, ", a[i]);
	printf("\b\b)\n");
}

void subtract(double *a, double *b, int n){
	for (i = 0; i < n; i++) a[i] = a[i] - b[i];
}

void copy(double *a, double *b, int n){
	for (i = 0; i < n; i++) a[i] = b[i];
}

void gramschmidt(double *a, double *b, double *c, int n){
	double dummy1[3], dummy2[3];

	copy(dummy1, b, 3);
	projection(dummy1, a, 3);
	subtract(b, dummy1, 3);
	
	copy(dummy1, c, 3);
	copy(dummy2, c, 3);
	projection(dummy1, a, 3);
	projection(dummy2, b, 3);
	
	subtract(c, dummy1, 3);
	subtract(c, dummy2, 3);
	
	normalize(a, 3);
	normalize(b, 3);
	normalize(c, 3);
}