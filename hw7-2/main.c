#include <stdio.h>
#include <stdlib.h>

#include "main.h"
#include "functions.h"

int main(){
	printf("Original vectors:\n");
	printvector(vector1, 3);
	printvector(vector2, 3);
	printvector(vector3, 3);
	
	gramschmidt(vector1, vector2, vector3, 3);
	
	printf("\nAfter Gram-Schmidt:\n");
	printvector(vector1, 3);
	printvector(vector2, 3);
	printvector(vector3, 3);
	
	printf("\n");
}
