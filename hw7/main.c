#include<stdio.h>
#include<stdlib.h>
#include "main.h"
#include "functions.h"

#define N 3
#define TRIALS 100

int main(){
	
	printf("Jacobi Method:\n\n");	
	jacobi( *A, *b, *x, N, N, TRIALS);

	zero(*x, N, 1);

	printf("\nGauss-Seidel Method:\n\n");
	gs( *A, *b, *x, N, N, TRIALS);
	
	zero(*x, N, 1);

	printf("\nGauss-Seidel Test to Fail:\n\n");
	gs( *test, *b, *x, N, N, TRIALS);
	return 0;
}
