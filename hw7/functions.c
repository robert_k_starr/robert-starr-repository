#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "functions.h"
#include "mkl.h"

#define EPSILON .001

void printmatrix(double *A, int rows, int columns){
	for (i = 0; i < rows; i++){
		for (j = 0; j < columns; j++) printf("%f\t", A[i*columns + j]);
		printf("\n");
	}
	printf("\n");
}

void copymatrix(double *copy, double *original, int rows, int columns){
	for (i = 0; i < rows; i++){
		for (j = 0; j < columns; j++) copy[i*columns + j] = original[i*columns + j];
	}
}

double norm_length(double *a, double *b, int n){
	double sum;
	int i;

	for (i = 0; i < n; i++) sum = sum + pow(a[i] - b[i], 2);
	
	return sqrt(sum);
}

void jacobi(double *A, double *b, double *x, int rows, int columns, int iterations){
	double D[rows][columns], R[rows][columns], x_k[rows][1];
	double sum, inverse;
	
	zero( *D, rows, columns);
	zero( *R, rows, columns);

	for (i = 0; i < rows; i++){
		for (j = 0; j < columns; j++){
			if (i == j)	D[i][j] = A[i*columns + j];
			else		R[i][j] = A[i*columns + j];
		}
	}
	for (k = 0; k < iterations; k++){
		for (i = 0; i < rows; i++){
			sum = 0;
			for (j = 0; j < rows; j++) sum = sum + R[i][j]*x[j];
			inverse = (double) 1/D[i][i];
			x_k[i][0] = inverse*(b[i] - sum);
		}
		if (norm_length(x, *x_k, 3) < EPSILON){
			printf("Converged in %i steps!\n\n", k);
			break;
		}
		copymatrix(x, *x_k, rows, 1);
	}

	printmatrix( x, rows, 1);
}

void gs(double *A, double *b, double *x, int rows, int columns, int iterations){
	double x_k[rows][1];
	double sum1, sum2;

	for (k = 0; k < iterations; k++){
		for (i = 0; i < rows; i++){
			sum1 = 0;
			sum2 = 0;
			for (j = 0; j < columns; j++){
				if (j < i) 	sum1 = sum1 + A[i*columns + j]*x_k[j][0];
				else if (j > i)	sum2 = sum2 + A[i*columns + j]*x[j];
			}
			x_k[i][0] = (double) 1/A[i*columns + i]*(b[i] - sum1 - sum2);
		}
		
		if (norm_length(x, *x_k, 3) < EPSILON){
			printf("Converged in %i steps!\n", k);
			break;
		}

		copymatrix( x, *x_k, rows, 1);
	}

	printmatrix( x, rows, 1);
}

void identity(double *I, int rows, int columns){
	zero(I, rows, columns);
	for (i = 0; i < rows; i++){
		I[i*columns + i] = 1;
	} 	
}

void zero(double *A, int rows, int columns){
	for (i = 0; i < rows; i++){
		for (j = 0; j < columns; j++) A[i*columns + j] = 0;
	}
}
