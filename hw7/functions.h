#ifndef FUNCTIONS_H
#define FUNCTIONS_H

int i, j, k;

void printmatrix(double *, int, int);
void normlength(double *, double *, int);
void jacobi(double *, double *, double *, int, int, int);
void gs(double *, double *, double *, int, int, int);
void identity(double *, int, int);
void zero(double *, int, int);
void copymatrix(double *, double *, int, int);

#endif /*FUNCTIONS_H*/
